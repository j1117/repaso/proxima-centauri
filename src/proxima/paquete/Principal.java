package proxima.paquete;

public class Principal {

	public static void main(String[] args) {
		
		// primer hito inicializar el array Declaraci�n e inicializaci�n de un array de enteros de tama�o 10
		// Introducci�n de valores en el array y visualizaci�n de los mismos: en cada posici�n el contenido ser� su n�mero de posici�n
		int contado1 = 0 ,contado2 = 0;	
		int numeros[];
		numeros = new int[10];
		
	for(int i =0 ; i < 10; i++) {
		numeros[i] = i;
		System.out.println("El array tiene el valor: " + numeros[i] + " en la posici�n: "  + i);
	}
		
		
		// segundo hito Modificaci�n del array y visualizaci�n del mismo: en las posiciones pares del array se modificar� el contenido que tenga por 0
	
	for(int i =0 ; i < 10; i++) {
		if(i%2 == 0) {			
			numeros [i] = 0;
			System.out.println("El n�mero en la posici�n " + i + " val�a: "+ i + " pero ahora vale: " + numeros [i]);
		}
	}
		
		
	// tercer hito Visualizaci�n del n�mero de ceros que hay en el array.
		// - Visualizaci�n del n�mero de elementos distintos de cero que hay en el array.
	
	for(int i =0 ; i < 10; i++) {
		if(numeros[i] == 0) {
			contado1 ++;
		}
	}
	System.out.println("hay un total de: " + contado1 + " ceros");
	
	for(int i =0 ; i < 10; i++) {
		if(numeros[i] != 0) {
			contado2 ++;
		}
	}
	System.out.println("hay un total de: " + contado2 + " elementos distintos a: '0'");
		}
	}

}
